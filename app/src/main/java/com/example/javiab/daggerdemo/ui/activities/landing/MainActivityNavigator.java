package com.example.javiab.daggerdemo.ui.activities.landing;

/**
 * Created by javiab on 9/6/2018.
 */

public interface MainActivityNavigator {

    void openLoginActivity();

    void openMainActivity();
}

