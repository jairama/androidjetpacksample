package com.example.javiab.daggerdemo.model.webapi;

import com.example.javiab.daggerdemo.model.webapi.request.LoginRequest;
import com.example.javiab.daggerdemo.model.webapi.response.LoginResponse;

import io.reactivex.Single;
import retrofit2.http.POST;

/**
 * Created by javiab on 9/6/2018.
 */

public interface WebAPI {
    @POST("bhavik/login")
    Single<LoginResponse> sendLoginRequest();
}
