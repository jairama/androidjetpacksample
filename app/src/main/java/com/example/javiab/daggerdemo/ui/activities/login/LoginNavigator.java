package com.example.javiab.daggerdemo.ui.activities.login;

/**
 * Created by javiab on 9/6/2018.
 */

public interface LoginNavigator {
    void handleError(Throwable throwable);

    void login();

    void openMainActivity();

    void setToastMessage(String message);
}
