package com.example.javiab.daggerdemo;

import android.app.Activity;
import android.app.Application;
import com.example.javiab.daggerdemo.di.DaggerDIComponent;
import javax.inject.Inject;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by javiab on 9/6/2018.
 */

public class DaggerApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerDIComponent.builder().application(this).build().inject(this);
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector(){
            return  dispatchingActivityInjector;
    }
}
