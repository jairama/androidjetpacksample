package com.example.javiab.daggerdemo.ui.activities.landing;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.javiab.daggerdemo.BR;
import com.example.javiab.daggerdemo.R;
import com.example.javiab.daggerdemo.databinding.ActivityMainBinding;
import com.example.javiab.daggerdemo.ui.activities.login.LoginActivity;
import com.example.javiab.daggerdemo.ui.base.BaseActivity;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;

import javax.inject.Inject;

/**
 * Created by javiab on 9/6/2018.
 */

public class MainActivity extends BaseActivity<ActivityMainBinding, MainActivityViewModel> implements MainActivityNavigator, HasFragmentInjector {
    @Inject
    MainActivityViewModel mainActivityViewModel;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Override
    public int getBindingVariable() {
        return BR.viewModelMainActivity;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainActivityViewModel getViewModel() {
        return mainActivityViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivityViewModel.setNavigator(this);
        mainActivityViewModel.decideNextActivity();
    }

    @Override
    public void openLoginActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = LoginActivity.newIntent(MainActivity.this);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }

    @Override
    public void openMainActivity() {

    }

    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return fragmentDispatchingAndroidInjector;

    }
}
