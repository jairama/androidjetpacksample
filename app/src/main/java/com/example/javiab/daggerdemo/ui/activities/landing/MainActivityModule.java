package com.example.javiab.daggerdemo.ui.activities.landing;

import com.example.javiab.daggerdemo.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by javiab on 9/6/2018.
 */

@Module
public class MainActivityModule {

    @Provides
    MainActivityViewModel provideMainActivityViewModel(SchedulerProvider schedulerProvider) {
        return new MainActivityViewModel(schedulerProvider);
    }
}
