package com.example.javiab.daggerdemo.ui.activities.login;

import com.example.javiab.daggerdemo.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Created by javiab on 9/6/2018.
 */

@Module
public class LoginActivityModule {

    @Provides
    LoginViewModel provideLoginViewModel(SchedulerProvider schedulerProvider) {
        return new LoginViewModel(schedulerProvider);
    }
}
