package com.example.javiab.daggerdemo.ui.activities.landing;

import com.example.javiab.daggerdemo.ui.base.BaseViewModel;
import com.example.javiab.daggerdemo.utils.rx.SchedulerProvider;

/**
 * Created by javiab on 9/6/2018.
 */

public class MainActivityViewModel extends BaseViewModel<MainActivityNavigator> {

    public MainActivityViewModel(SchedulerProvider mSchedulerProvider) {
        super(mSchedulerProvider);
    }

    public void decideNextActivity() {
        getNavigator().openLoginActivity();
    }
}
