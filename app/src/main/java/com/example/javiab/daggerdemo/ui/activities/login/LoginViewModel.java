package com.example.javiab.daggerdemo.ui.activities.login;


import android.text.TextUtils;

import com.example.javiab.daggerdemo.model.Users;
import com.example.javiab.daggerdemo.model.webapi.WebAPI;
import com.example.javiab.daggerdemo.model.webapi.request.LoginRequest;
import com.example.javiab.daggerdemo.model.webapi.response.LoginResponse;
import com.example.javiab.daggerdemo.ui.base.BaseViewModel;
import com.example.javiab.daggerdemo.utils.CommonUtils;
import com.example.javiab.daggerdemo.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import retrofit2.Retrofit;

/**
 * Created by javiab on 9/6/2018.
 */

public class LoginViewModel extends BaseViewModel<LoginNavigator> {
    private Users user;

    public LoginViewModel(SchedulerProvider mSchedulerProvider) {
        super(mSchedulerProvider);
        user = new Users("", "");
    }

    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";

    public boolean isEmailAndPasswordValid(String email, String password) {
        // validate email and password
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        if (!CommonUtils.isEmailValid(email)) {
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            return false;
        }
        return true;
    }
//    @Bindable
//    public String toastMessage = null;


//    public String getToastMessage() {
//        return toastMessage;
//    }


//    private void setToastMessage(String toastMessage) {
//        this.toastMessage = toastMessage;
//        notifyPropertyChanged(BR.toastMessage);
//    }


    public void afterEmailTextChanged(CharSequence s) {
        user.setEmail(s.toString());
    }

    public void afterPasswordTextChanged(CharSequence s) {
        user.setPassword(s.toString());
    }

    public void onLoginClicked() {
        if (user.isInputDataValid()) {
//            validateUser();
            getNavigator().setToastMessage(successMessage);

        } else
            getNavigator().setToastMessage(errorMessage);
    }

    public void validateUser(WebAPI mWebAPI) {

        getCompositeDisposable().add(mWebAPI.sendLoginRequest()
                .doOnSuccess(response -> new LoginResponse(response.getEmail(), response.getPassword()))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().openMainActivity();
                }, throwable -> {
                    setIsLoading(false);
                    getNavigator().handleError(throwable);
                })
        );
    }
}
