package com.example.javiab.daggerdemo.di;


import android.app.Application;
import android.content.Context;

import com.example.javiab.daggerdemo.model.webapi.WebAPI;
import com.example.javiab.daggerdemo.utils.rx.AppSchedulerProvider;
import com.example.javiab.daggerdemo.utils.rx.SchedulerProvider;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import dagger.Reusable;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by javiab on 9/6/2018.
 */

@Module
public class DaggerModule {

    @Provides
    @Singleton
    public Context provideContext(Application application) {
        return application;
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://xz2vl.mocklab.io/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    WebAPI provideWebAPIService(Retrofit retrofit) {
        return retrofit.create(WebAPI.class);
    }
}
