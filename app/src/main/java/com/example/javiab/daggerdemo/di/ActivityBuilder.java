package com.example.javiab.daggerdemo.di;

import com.example.javiab.daggerdemo.ui.activities.landing.MainActivity;
import com.example.javiab.daggerdemo.ui.activities.landing.MainActivityModule;
import com.example.javiab.daggerdemo.ui.activities.login.LoginActivity;
import com.example.javiab.daggerdemo.ui.activities.login.LoginActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by javiab on 9/6/2018.
 */

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    public abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {LoginActivityModule.class})
    public abstract LoginActivity bindLoginActivity();


}
