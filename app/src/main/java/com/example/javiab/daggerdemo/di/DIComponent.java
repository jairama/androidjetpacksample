package com.example.javiab.daggerdemo.di;

import android.app.Application;

import com.example.javiab.daggerdemo.DaggerApplication;
import com.example.javiab.daggerdemo.model.webapi.response.LoginResponse;
import com.example.javiab.daggerdemo.ui.activities.landing.MainActivity;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

/**
 * Created by javiab on 9/6/2018.
 */

@Singleton
@Component(modules = {DaggerModule.class, AndroidInjectionModule.class, ActivityBuilder.class})
public interface DIComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        DIComponent build();

    }
    void inject(DaggerApplication application);
}
