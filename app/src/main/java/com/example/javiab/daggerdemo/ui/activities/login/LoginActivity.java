package com.example.javiab.daggerdemo.ui.activities.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.javiab.daggerdemo.BR;
import com.example.javiab.daggerdemo.R;
import com.example.javiab.daggerdemo.databinding.ActivityLoginBinding;
import com.example.javiab.daggerdemo.model.webapi.WebAPI;
import com.example.javiab.daggerdemo.ui.base.BaseActivity;

import javax.inject.Inject;

import retrofit2.Retrofit;

/**
 * Created by javiab on 9/6/2018.
 */

public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginViewModel> implements LoginNavigator{

    @Inject
    LoginViewModel mLoginViewModel;

    @Inject
    Retrofit retrofit;

    @Inject
    WebAPI mWebAPI;

    private ActivityLoginBinding mActivityLoginBinding;

    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public LoginViewModel getViewModel() {
        return mLoginViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        // handle error
    }

    @Override
    public void login() {
        String email = mActivityLoginBinding.inEmail.getText().toString();
        String password = mActivityLoginBinding.inPassword.getText().toString();
        if (mLoginViewModel.isEmailAndPasswordValid(email, password)) {
            hideKeyboard();
        } else {
            Toast.makeText(this, getString(R.string.invalid_email_password), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void openMainActivity() {
    }

    @Override
    public void setToastMessage(String message) {
        mLoginViewModel.validateUser(mWebAPI);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityLoginBinding = getViewDataBinding();
        mLoginViewModel.setNavigator(this);
    }
}

